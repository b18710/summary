<?php

/*=============== Session 1 ===============*/


//Activity 1
function getFullAddress($country, $city, $province, $specificAddress) {
	return "$specificAddress, $city, $province, $country";
}


//Activity 2
function getLetterGrade($grade) {
	if ($grade >= 98 && $grade <= 100) {
		return "$grade is equivalent to A+";
	} else if ($grade >= 95 && $grade <= 97) {
		return "$grade is equivalent to A";
	} else if ($grade >= 92 && $grade <= 94) {
		return "$grade is equivalent to A-";
	} else if ($grade >= 89 && $grade <= 91) { 
		return "$grade is equivalent to B+";
	} else if ($grade >= 86 && $grade <= 88) { 
		return "$grade is equivalent to B";
	} else if ($grade >= 83 && $grade <= 85) { 
		return "$grade is equivalent to B-";
	} else if ($grade >= 80 && $grade <= 82) { 
		return "$grade is equivalent to C+";
	} else if ($grade >= 77 && $grade <= 79) { 
		return "$grade is equivalent to C";
	} else if ($grade >= 75 && $grade <= 76) { 
		return "$grade is equivalent to C-";
	} else if ($grade < 75 ) { 
		return "$grade is equivalent to D";
	}
}



/*=============== Session 2 ===============*/


function Loop() {
	for($count=0; $count <= 100; $count++){
		if ($count % 5 === 0) {
			echo $count . ", ";
		}
	}
}



/*=============== Session 3 ===============*/



class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName() {
		return "Your full name is $this->firstName $this->lastName.";
	}
}

class Developer extends Person {

	public function printName() {
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
}

class Engineer extends Person {

	public function printName() {
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}

$person1 = new Person('Senku','','Ishigami');
$person2 = new Developer('John', 'Finch', 'Smith');
$person3 = new Engineer('Harold', 'Myers', 'Reese');



/*=============== Session 4 ===============*/

class Building {
	protected $name;
	private $floors;
	private $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}
}


class Condominium extends Building {

}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
