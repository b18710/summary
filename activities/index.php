<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>a1 Activity</title>
</head>
<body>

<!-- +++++++++ Session 1 +++++++++ -->


	<!-- Activity 1 -->
	<h1>Full Address</h1>
	<p><?php echo getFullAddress("Philippines", "Quezon City", "Metro Manila", "3F Caswynn Bldg., Timog Avenue"); ?></p>
	<p><?php echo getFullAddress("Philippines", "Makati City", "Metro Manila", "3F Enzo Bldg., Buendia Avenue"); ?></p>

	<!-- Activity 2 -->
	<h1>Letter-Base Grading</h1>
	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>


<!-- +++++++++ Session 2 +++++++++ -->


	<h1>Activity 1</h1>
	<?php Loop(); ?>

	<h1>Activity 2</h1>
	
	<?php $students = array(); ?>

	<?php array_push($students,'John Smith'); ?>
	<p><?php echo var_dump($students); ?></p>
	<p><?= count($students); ?></p>

	<?php array_push($students,'Jane Smith'); ?>
	<p><?php echo var_dump($students); ?></p>
	<p><?= count($students); ?></p>

	<?php array_shift($students); ?>
	<p><?php echo var_dump($students); ?></p>
	<p><?= count($students); ?></p>



<!-- +++++++++ Session 3 +++++++++ -->


	<h1>Person</h1>
	<p><?= $person1->printName(); ?></p>

	<h1>Developer</h1>
	<p><?= $person2->printName(); ?></p>

	<h1>Engineer</h1>
	<p><?= $person3->printName(); ?></p>

<!-- +++++++++ Session 4 +++++++++ -->

	<h1>Building</h1>
	<p>The name of the building is <?php echo $building->getName(); ?>.</p>
	<p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloors(); ?> floors.</p>
	<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>
	<?php $building->setName('Caswynn Complex'); ?>
	<p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

	<h1>Condominium</h1>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloors(); ?> floors.</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>
	<?php $condominium->setName('Enzo Tower'); ?>
	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>

</body>
</html>