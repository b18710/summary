<!-- PHP code can be included in another file by using the require_once keyword -->
<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S01</title>
</head>
<body>


<!-- +++++++++ PHP Basics and Selection Control Structures +++++++++ -->


	<h1>Variables</h1>
	<!-- Variables can be used to output data indouble quotes while single quotes will not work -->
	<p><?php echo "$name"; ?></p>
 	<p><?php echo "Good day $name! Your email is $email."; ?></p>
 	<p><?php echo PI; ?></p>
 	<p><?php echo $address; ?></p>

 	<h1>Data Types</h1>
 	<!-- Normal echoing of boolean and null variables will not make them visible to the user. To see their types instead, we can use the following -->
 	<p><?php echo $hasTravelledAbroad;?></p>
 	<p><?php echo $spouse; ?></p>
 	<p><?php echo gettype($hasTravelledAbroad); ?></p>
 	<p><?php echo gettype($spouse); ?></p>

 	<!-- To see more detailed info on the variable, use var_dump -->
 	<p><?php echo var_dump($hasTravelledAbroad); ?></p>
 	<p><?php echo var_dump($spouse); ?></p>

 	<!-- echoing an array -->
 	<p><?php echo print_r($grades); ?></p>
 	<p><?php echo var_dump($grades); ?></p>
 	<p><?php echo $grades[3]; ?></p>


 	<!-- echoing objects -->
 	<!-- Use the -> notation to access the properties of an object -->
 	<p><?php echo $gradesObj->firstGrading; ?></p>
 	<p><?php echo $personObj->address->state; ?></p>
 	<p><?php echo print_r($gradesObj); ?></p>
 	<p><?php echo var_dump($gradesObj); ?></p>

 	<h1>Operators</h1>

 	<h3>Arithmetic Operators</h3>
 	<p>Sum: <?php echo $x + $y; ?></p>
 	<p>Difference: <?php echo $x - $y; ?></p>
 	<p>Product: <?php echo $x * $y; ?></p>
 	<p>Quotient: <?php echo $x / $y; ?></p>

 	<h3>Equality Operators</h3>
 	<p>Loose Equality: <?php echo var_dump($x == 234); ?></p>
 	<p>Loose Inequality: <?php echo var_dump($x != 123); ?></p>
 	<p>Strict Equality: <?php echo var_dump($x == 123); ?></p>
 	<p>Strict Inequality: <?php echo var_dump($x !== 234); ?></p>

 	<h3>Greater/Leseer Operators</h3>
 	<p>Is Lesser (or equal): <?php echo var_dump($x < $y) ?></p>
 	<p>Is Lesser (or equal): <?php echo var_dump($x >= $y) ?></p>

 	<h3>Logical Operators</h3>
 	<p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered) ?></p>
 	<p>Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered) ?></p>
 	<p>Are Some Requirements Not Met: <?php var_dump(!$isLegalAge && !$isRegistered) ?></p>

 	<h1>Functions</h1>
 	<p>Full Name: <?php echo getFullName("John","J","Smith"); ?></p>

 	<h1>Selection Control Structures</h1>

 	<h3>If-Elseif-Else</h3>
 	<p><?php echo determineTyphoonIntensity(63); ?></p>

 	<h3>Ternary Operator</h3>
 	<p><?php var_dump(isUnderAge(78)); ?></p>
 	<p><?php var_dump(isUnderAge(13)); ?></p>

 	<h3>Switch Statement</h3>
 	<p><?php echo determineUser(2); ?></p>



 <!-- +++++++++ Selection Control Structures and Array Manipulation +++++++++ -->

 	<h1>Repetition Control Structures</h1>
 	<h3>While Loop</h3>
 	<?php whileLoop(); ?>

 	<h3>Do-While Loop</h3>
 	<?php doWhileLoop(); ?>

 	<h3>For Loop</h3>
 	<?php forLoop(); ?>

 	<h1>Array Manipulation</h1>

 	<h2>Types of Arrays</h2>

 	<h3>Simple Array</h3>
 	<ul>
 		<?php foreach($grades as $grade){ ?>
 			<li><?php echo $grade; ?></li>
 		<?php } ?>
 	</ul>

 	<h3>Associative Array</h3>
 	<ul>
 		<?php foreach($gradePeriods as $period => $grade){ ?>
 			<li>Grade in <?= $period; ?> is <?= $grade; ?></li>
 		<?php } ?>

 		<!-- comment in note -->
 	</ul>

 	<h3>Multi-Dimensional Array</h3>
 	<ul>
 		<?php
 			foreach($heroes as $team){
 				foreach($team as $member){
 					?>
 						<li><?= $member; ?></li>
 					<?php }	
 			}
 		?>
 	</ul>

 	<h4>Using for loop</h4>
 		<?php
 			for($i = 0; $i < count($heroes); $i++){
 				for($j = 0; $j < count($heroes[$i]); $j++){
 					echo $heroes[$i][$j]; ?> </br>
 					<?php
 				}
 			}
 		?>


 	<h3>Multi-Dimensional Associative Array</h3>

 	<?php
 		foreach($powers as $label => $powerGroup){
 			foreach($powerGroup as $power){
 				?>
 					<li><?= "$label: $power"; ?></li>
 				<?php }
 			}
 	?>
 	
 	<h3>Add to Array</h3>
 	
 	<?php array_push($computerBrands, 'Apple'); ?>
 	<pre><?php print_r($computerBrands); ?></pre>

 	<?php array_unshift($computerBrands, 'Del'); ?>
 	<pre><?php print_r($computerBrands); ?></pre>

 	<h3>Remove From Array</h3>
 	<?php array_pop($computerBrands); ?>
 	<pre><?php print_r($computerBrands); ?></pre>

 	<?php array_shift($computerBrands); ?>
 	<pre><?php print_r($computerBrands); ?></pre>

 	<h3>Sort/Reverse</h3>

 	<?php sort($computerBrands); ?>
 	<pre><?php print_r($computerBrands); ?></pre>

 	<?php rsort($computerBrands); ?>
 	<pre><?php print_r($computerBrands); ?></pre>

 	<h3>Count</h3>
 	<p><?= count($computerBrands); ?></p>

 	<h3>In Array</h3>
 	<p><?= searchBrand('HP', $computerBrands); ?></p>


 <!-- +++++++++ Classes, Objects, Inheritance and Polymorphism +++++++++ -->


	 <h1>Objects from Variables</h1>
	 <p><?php var_dump($buildingObj); ?></p>

	 <h1>Objects from Classes</h1>
	 <p><?php var_dump($building); ?></p>
	 <p><?php var_dump($building2); ?></p>
	 <p><?= $building->printName(); ?></p>
	 <p><?= $building2->printName(); ?></p>

	 <h1>Inheritance</h1>
	 <p><?= $condominium->floors; ?></p>
	 <p><?= $condominium->address; ?></p>

	 <h1>Polymorphism</h1>
	 <p><?= $condominium->printName(); ?></p>

 <!-- +++++++++ Access Modifiers and Encapsulation +++++++++ -->

 	<h1>Access Modifiers</h1>

 	<h3>Building</h3>
 	<!-- <php $building->name = "Changed Name"; ?> -->
 	<!-- <p><= $building->name; ?></p> -->

 	<h3>Condominium</h3>
 	<!-- <p><= $condominium->name; ?></p> -->

 	<h3>Encapsulation</h3>
 	<?php $condominium->setName('Enzo Tower'); ?>
 	<?php echo $condominium->getName(); ?>


</body>
</html>