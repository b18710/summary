<?php


/*=============== PHP Basics and Selection Control Structures ===============*/



//[SECTION] Comments

//This is a single-line comment

#This is also a single-line comment

/*PHP Comments are written exactly the same as JS comments. This is a multiple-lines comment block*/

/*
When writing any PHP code, always remember to start with the opening php tag:

<?php

Closing with ?> is optional when ONLY writing PHP code
*/

//echo is used to output data to the screen
//always remember to end every PHP statement with a semicolon(;)
//echo "Mabuhay World";

//[SECTION] Variables

$name = "John Smith";
$email = "johnsmith@mail.com";

//[SECTION] Constants

define('PI', 3.1416);

$PI = 2;

//[SECTION] Data Types
$state ="New York";
$country = "USA";
$address = $state . ', ' . $country; //concatenation via dot
$address2 = "$state, $country"; //concatenation via double quotes

//Integers
$age = 31;
$headcount = 26;

//Floats
$grade = 98.2;
$distance = 1342.12;

//Boolean
$hasTravelledAbroad = true;

//Null
$spouse = null;

//Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6,
];

$personObj = (object)[
	'fullName' => "John Smith",
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'New York',
		'country' => "USA"
	]
];


//[SECTION] Operators

//Assignment OPerator (=)
$x = 234;
$y = 123;

$isLegalAge = true;
$isRegistered = false;


//[SECTION] Functions
function getFullName($firstName, $middleInitial, $lastName) {
	return "$lastName, $firstName, $middleInitial";
}

//[SECTION] Selection Control Structures

//If-Elseif-Else Statement
function determineTyphoonIntensity($windSpeed) {
	if ($windSpeed < 30) {
		return "Not a typhoon yet";
	} else if ($windSpeed <= 61) {
		return "Tropical depression detected";
	} else if ($windSpeed >= 62 && $windSpeed <= 88) {
		return "Severe tropical storm detected";
	} else if ($windSpeed >= 89 && $windSpeed <= 117) { 
		return "Severe tropical storm detected";
	} else {
		"Typhoon detected";
	}
}

//Ternary Operator
function isUnderAge($age) {
	return ($age < 18) ? true : false;
}

//Switch Statement
function determineUser($computerNumber) {
	switch ($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		default:
			return "The computer number $computerNumber isour of bounds.";
			break;
	}
}



/*=============== Selection Control Structures and Array Manipulation ===============*/


/*
[SECTION] Repetition Control Structures

While Loop:

A while loop takes a single condition. FOr as long as the condition is true, the code inside the block will run
*/

function whileLoop() {
	$count = 0;

	while($count <= 5){
		echo $count . '</br>';
		$count++;
	}
}

/*
Do-While Loop
A do-while loop is guaranteed to run at least once before conditions are checked.
*/

function doWhileLoop() {
	$count = 20;

	do{
		echo $count . '</br>';
		$count--;
	}while($count > 0);
}

/*
For Loop

A for loop is a more flexible kind of loop. It consists of the following three parts:
	-The initial value that will track the progression of the loop
	-The condition that will be evaluated and will determine whether the loop will continue running or not
	-The iteration method that indicates how to advance the loop

*/

function forLoop(){
	for($i=0; $i <= 10; $i++){
		echo $i . "</br>";
	}
}


//[SECTION] Array Manipulation

$studentNumbers = array('1923', '1924', '1925', '1926');

//Simple array
//$studentNumbers = ['1923', '1924', '1925', '1926'];
$grades = [98.5, 94.3, 89.2, 90.1];

//Associative Arrays
$gradePeriods = ['firstGrading' => 98.5, 'secondGradin' => 94.3, 'thirdGrading' => 90.1];


//Multi-Dimensional Arrays
$heroes = [
	['Iron Man','Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Wonder Woman'],
	['Batman', 'Superman', 'Wonder Woman']
];


//Multi-Dimensional Associative Arrays

$powers = [
	'regular' => ['Repulsor Blast', 'Rocket Puch'],
	'signature' => ['Unibeam']
];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Searching arrays:
function searchBrand($brand, $brandsArr){
	if(in_array($brand, $brandsArr)){
			return "$brand is in the array.";
	} else {
		return "$brand is NOT in the array.";
	}
}


//strtolower(String)



/*=============== Classes, Objects, Inheritance and Polymorphism ===============*/


//[SECTION] Objects as variables

$buildingObj = (object)[
	'name' =>'Caswyn Building',
	'floors' => 8,
	'address' => (object) [
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

//[SECTION] Object from Classes

class Building {
	public $name;
	public $floors;
	public $address;

	//constructor
	//A constructor is used during the creation of an object to provide the initial values of each property

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//methods
	public function printName() {
		return "The name of the building is $this->name";
	}
}

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$building2 = new Building('Manulife Building', 51, 'Commonwealth Avenue, Quezon City, Philippines');


//[SECTION] Inheritance and Polymorphism

class Condominium extends Building {
	//$name, $floors, and $addressare inherited from the Building class to this class
	//It means that condominiums also have a name,floors, address property, as well as the printName() method

	public function printName() {
		return "The name of this condominium is $this->name";
	}
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');


/*=============== Access Modifiers and Encapsulation ===============*/

//[SECTION] Access Modifiers

//Each property and method inside of a class can be given a certain access modifier

//PUBLIC means that the property/method is accessible to all and can be reassigned/changed by anyone

//PRIVATE means that direct access to an object's propertyis disabled and cannot be changed by anyone
//it also means that inheritance of its properties and methods is also disbaled

//PROTECTED also disables direct access to an object's properties and methods but inheritance is still allowed
//take note that the protected access modifier is ALSO inherited to the child class

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
}

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');


//[SECTION] Encapsulation
//Using what are called getter methods and setter methods,we can implement encapsulation of an object's data
//Getters and setters serve as an intermediary in accessing or reassigning an object's properties or methods
//Getters and setters work for both private and protected properties
//You do not always need a getter and setter for every property

class Condominium extends Building {
	
	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}


}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
